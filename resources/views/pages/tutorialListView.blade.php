@extends('fullWidthLayout')
@section('content')
<div class="row search mt-5 mb-5">
    <div class="col-md-12">
        <div class="w-100 d-md-flex align-items-center">
            <!-- Filter: Category -->
            <div class="d-flex align-items-center justify-content-center course-filter mr-5">
                <div class="filter-label mr-2">
                Category:
                </div>
                <div class="btn-group">
                <button class="btn btn-default btn-lg btn-course-filter dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                    All <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/courses">All</a></li>
                    
                </ul>
                </div>
            </div>
            <!-- Filter: Author -->
            <div class="d-flex align-items-center justify-content-center  course-filter">
                <div class="filter-label mr-2">
                Author:
                </div>
                <div class="btn-group">
                <button class="btn btn-default btn-lg btn-course-filter dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                    
                    All <span class="caret"></span>
                    
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/courses">All</a></li>
                    
                    <li><a href="/courses/author/518379">Angel Estrada</a></li>
                    
                    <li><a href="/courses/author/76028">Dr. Mojibul Haque</a></li>
                    
                    <li><a href="/courses/author/537298">Edwin Uribe</a></li>
                    
                    <li><a href="/courses/author/537371">Fatima Mora</a></li>
                    
                    <li><a href="/courses/author/662376">Irma Felix</a></li>
                    
                    <li><a href="/courses/author/1213144">Sofia Mora</a></li>
                    
                    <li><a href="/courses/author/541845">Yesenia Cortés</a></li>
                    
                </ul>
                </div>
            </div>
            
            <!-- Search Box -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ml-md-auto">
                <form role="search" method="get" action="/courses">
                    <div class="input-group">
                        <label for="search-courses" class="sr-only">Find a product</label>
                        <input class="form-control search input-lg" data-list=".list" id="search-courses" name="query" placeholder="Find a product" type="text">
                    <span class="input-group-btn">
                        <button aria-label="Search Courses" id="search-course-button" class="btn search btn-default btn-lg mb-0" type="submit"><i class="fa fa-search" title="Search"></i></button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="roww">
    <div class="col-md-12 p-0">
        <hr />
    </div>
</div>
<div class="row course-list list">
    <!-- Course Listing -->
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="card mt-4 mb-4 ">
            <img class="card-img-top" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw" alt="Card image cap">
            <div class="card-body p-5">
                <a href="#" class="card-title course-listing-title">Nutrition & Integrative Medicine for Diabetes, Cognitive Decline & Alzheimer’s Disease</a>
                <div class="progress mt-3">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="course-listing-extra-info" aria-hidden="false">
                    <div>
                      <!-- Bundle Info (everyone) -->
                        <!-- Author Image and Name (everyone) -->
                        <img class="img-circle max-h-30px" src="{{ asset('assets/images/m-mojibul-.jpg') }}" alt="Dr. Mojibul Haque">
                        <span class="small course-author-name">
                          Dr. Mojibul Haque
                        </span>
                    </div>
                    <!-- Progress percentage (enrolled users) -->
                    <div>
                        <div class="" aria-hidden="false">
                            <div class="small course-progress">
                              <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                              <br>
                              COMPLETE
                            </div>
                        </div>
                          <!-- Price (unenrolled users) -->
                    </div>
                  </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="card mt-4 mb-4 ">
            <img class="card-img-top" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw" alt="Card image cap">
            <div class="card-body p-5">
                <a href="#" class="card-title course-listing-title">Nutrition & Integrative Medicine for Diabetes, Cognitive Decline & Alzheimer’s Disease</a>
                <div class="progress mt-3">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="course-listing-extra-info" aria-hidden="false">
                    <div>
                      <!-- Bundle Info (everyone) -->
                        <!-- Author Image and Name (everyone) -->
                        <img class="img-circle max-h-30px" src="{{ asset('assets/images/m-mojibul-.jpg') }}" alt="Dr. Mojibul Haque">
                        <span class="small course-author-name">
                          Dr. Mojibul Haque
                        </span>
                    </div>
                    <!-- Progress percentage (enrolled users) -->
                    <div>
                        <div class="" aria-hidden="false">
                            <div class="small course-progress">
                              <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                              <br>
                              COMPLETE
                            </div>
                        </div>
                          <!-- Price (unenrolled users) -->
                    </div>
                  </div>
            </div>
        </div>
    </div><div class="col-xs-12 col-sm-6 col-md-4">
        <div class="card mt-4 mb-4 ">
            <img class="card-img-top" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw" alt="Card image cap">
            <div class="card-body p-5">
                <a href="#" class="card-title course-listing-title">Nutrition & Integrative Medicine for Diabetes, Cognitive Decline & Alzheimer’s Disease</a>
                <div class="progress mt-3">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="course-listing-extra-info" aria-hidden="false">
                    <div>
                      <!-- Bundle Info (everyone) -->
                        <!-- Author Image and Name (everyone) -->
                        <img class="img-circle max-h-30px" src="{{ asset('assets/images/m-mojibul-.jpg') }}" alt="Dr. Mojibul Haque">
                        <span class="small course-author-name">
                          Dr. Mojibul Haque
                        </span>
                    </div>
                    <!-- Progress percentage (enrolled users) -->
                    <div>
                        <div class="" aria-hidden="false">
                            <div class="small course-progress">
                              <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                              <br>
                              COMPLETE
                            </div>
                        </div>
                          <!-- Price (unenrolled users) -->
                    </div>
                  </div>
            </div>
        </div>
    </div><div class="col-xs-12 col-sm-6 col-md-4">
        <div class="card mt-4 mb-4 ">
            <img class="card-img-top" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw" alt="Card image cap">
            <div class="card-body p-5">
                <a href="#" class="card-title course-listing-title">Nutrition & Integrative Medicine for Diabetes, Cognitive Decline & Alzheimer’s Disease</a>
                <div class="progress mt-3">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="course-listing-extra-info" aria-hidden="false">
                    <div>
                      <!-- Bundle Info (everyone) -->
                        <!-- Author Image and Name (everyone) -->
                        <img class="img-circle max-h-30px" src="{{ asset('assets/images/m-mojibul-.jpg') }}" alt="Dr. Mojibul Haque">
                        <span class="small course-author-name">
                          Dr. Mojibul Haque
                        </span>
                    </div>
                    <!-- Progress percentage (enrolled users) -->
                    <div>
                        <div class="" aria-hidden="false">
                            <div class="small course-progress">
                              <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                              <br>
                              COMPLETE
                            </div>
                        </div>
                          <!-- Price (unenrolled users) -->
                    </div>
                  </div>
            </div>
        </div>
    </div><div class="col-xs-12 col-sm-6 col-md-4">
        <div class="card mt-4 mb-4 ">
            <img class="card-img-top" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw" alt="Card image cap">
            <div class="card-body p-5">
                <a href="#" class="card-title course-listing-title">Nutrition & Integrative Medicine for Diabetes, Cognitive Decline & Alzheimer’s Disease</a>
                <div class="progress mt-3">
                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="course-listing-extra-info" aria-hidden="false">
                    <div>
                      <!-- Bundle Info (everyone) -->
                        <!-- Author Image and Name (everyone) -->
                        <img class="img-circle max-h-30px" src="{{ asset('assets/images/m-mojibul-.jpg') }}" alt="Dr. Mojibul Haque">
                        <span class="small course-author-name">
                          Dr. Mojibul Haque
                        </span>
                    </div>
                    <!-- Progress percentage (enrolled users) -->
                    <div>
                        <div class="" aria-hidden="false">
                            <div class="small course-progress">
                              <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                              <br>
                              COMPLETE
                            </div>
                        </div>
                          <!-- Price (unenrolled users) -->
                    </div>
                  </div>
            </div>
        </div>
    </div>
    
</div>
@endsection