@extends('sideMenuLayout')
@section('content')
<div role="main" class="course-mainbar">
   <!-- Meta tag for tracking lecture progress -->
   <meta id="lecture-completion-data" data-last-lecture-id="19207135" data-last-lecture-url="/courses/856463/lectures/19207135" data-is-completed="true" data-can-access-lecture="true" data-compliance-enabled="false" data-valid-for-completion="true">
   <h2 id="lecture_heading" class="section-title dsp-flex-xs flex-align-items-center-xs" data-lecture-id="18234735" data-next-lecture-id="18234740" data-lecture-url="/courses/856463/lectures/18234735" data-next-lecture-url="/courses/856463/lectures/18234740" ,="" data-previous-lecture-url="/courses/856463/lectures/20025579" data-previous-lecture-id="20025579">
      <svg width="24" height="24">
         <use xlink:href="#icon__Video"></use>
      </svg>
      &nbsp;
      Introduction Part 1
   </h2>
   <!-- Attachment Blocks -->
   <div class="lecture-attachment lecture-attachment-type-video" id="lecture-attachment-40678224">
      <div class="attachment-data"></div>
      <!-- Attachment: Video -->
      <div class="wistia_responsive_padding">
         <div class="wistia_responsive_wrapper">
            <div class="wistia_video_foam_dummy">
            </div>
         </div>
      </div>
      <!-- Video actions -->
   </div>
   <div class="lecture-attachment lecture-attachment-type-audio" id="lecture-attachment-36445473">
      <div class="attachment-data"></div>
      <div class="audioloader" data-audioloader="AttachmentDrop" data-audioloader-name="Introduction to Integrative Medicine and Nutrition - 01.mp3" data-audioloader-type="audio/mpeg" data-audioloader-url="https://cdn.fs.teachablecdn.com/ur9GThzQrOxR0GWDmZH6" data-audioloader-initialized="true">
         <div class="audioloader__placeholder">
            <button data-href="https://cdn.fs.teachablecdn.com/ur9GThzQrOxR0GWDmZH6" target="_blank" style="
               border: 0;
               outline: 0;
               background: transparent;
               cursor: pointer;
               ">
            <span class="audioloader__icon glyphicon glyphicon-play"></span>
            <span class="audioloader__name">Introduction to Integrative Medicine and Nutrition - 01.mp3</span>
            </button>
         </div>
      </div>
      <div class="video-options">
         <a class="download" href="https://cdn.fs.teachablecdn.com/ur9GThzQrOxR0GWDmZH6" target="_blank" rel="noopener" download="" data-x-origin-download="" data-x-origin-download-name="Introduction to Integrative Medicine and Nutrition - 01.mp3" data-x-origin-download-initialzed="true">
            <div class="span glyphicon glyphicon-save"></div>
            Download
         </a>
      </div>
   </div>
   <div class="lecture-attachment lecture-attachment-type-native_comments" id="lecture-attachment-39345934">
      <div class="attachment-data"></div>
   </div>
   <div class="lecture-attachment lecture-attachment-type-pdf_embed" id="lecture-attachment-54268604">
      <div class="attachment-data"></div>
      <div class="download-pdf">
         <!-- Attachment: File -->
         <div class="row attachment">
            Download
         </div>
         <div class="row attachment">
            <a class="download" href="https://cdn.fs.teachablecdn.com/dwYfFrzFSUGmAeqCoq45" target="_blank" rel="noopener" download="" data-x-origin-download="" data-x-origin-download-name="Slides - Module 1 - Introduction to Integrative Medicine and Nutrition.pdf" data-x-origin-download-initialzed="true">
               <svg width="24" height="24" class="icon-m v-middle" aria-label="Download">
                  <use xlink:href="#icon__Attachment"></use>
               </svg>
               Slides - Module 1 - Introduction to Integrative Medicine and Nutrition.pdf
            </a>
         </div>
      </div>
   </div>
   <!-- Comments -->
   <div class="lecture-attachment">
      <div class="comments attachment-block-wrapper">
         <div id="comments_settings" class="comments-settings hidden" data-thread-endpoint="/api/v1/comments/?commentable_type=Attachment&amp;commentable_id=39345934&amp;threaded=true" data-i18n="{&quot;post_title&quot;:&quot;Post a comment&quot;,&quot;post_placeholder&quot;:&quot;Leave a comment...&quot;,&quot;post_success&quot;:&quot;Your comment was posted.&quot;,&quot;post_moderation&quot;:&quot;Your comment was posted, but it needs to be approved by the school owner before it shows up.&quot;,&quot;post_fail&quot;:&quot;Sorry, your comment could not be posted at this time :(&quot;,&quot;post_comment&quot;:&quot;Post Comment&quot;,&quot;save_comment&quot;:&quot;Save Comment&quot;,&quot;view_comment&quot;:&quot;View comment&quot;,&quot;reply&quot;:&quot;Reply&quot;,&quot;ago&quot;:&quot;ago&quot;,&quot;now&quot;:&quot;now&quot;,&quot;instructor&quot;:&quot;Instructor&quot;,&quot;awaiting_review&quot;:&quot;Awaiting Review&quot;,&quot;comments&quot;:&quot;comments&quot;,&quot;notifications_participating_thread&quot;:&quot;Notify me when someone comments on a discussion I've commented in.&quot;,&quot;notifications_responses&quot;:&quot;Notify me when someone responds to my comments.&quot;,&quot;notifications_author&quot;:&quot;Notify me when someone comments in one of my courses.&quot;,&quot;has_been_removed&quot;:&quot;This comment has been removed.&quot;,&quot;add_image&quot;:&quot;Add Image&quot;,&quot;view_thread&quot;:&quot;View the rest of this thread&quot;,&quot;approve&quot;:&quot;Approve&quot;,&quot;edit&quot;:&quot;Edit&quot;,&quot;delete&quot;:&quot;Delete&quot;,&quot;permalink&quot;:&quot;Link&quot;,&quot;load_more&quot;:&quot;Load more&quot;,&quot;add_text_error&quot;:&quot;Please add text to the comment.&quot;,&quot;comment_responses_email&quot;:&quot;Email me when someone responds to my comments.&quot;,&quot;comment_discussion_email&quot;:&quot;Email me when someone comments on a discussion I've commented in.&quot;}" data-attachments-enabled="true" data-filepicker-key="ADNupMnWyR7kCWRvm76Laz"></div>
         <h4 class="comments__heading">Post a comment</h4>
         <div class="comments__wrapper">
            <div class="comments__block comments__block--indent-level-0 comments__block--student comments__block--new" id="new_comment_container">
               <div class="commenter-profile">
                  <img class="gravatar" src="https://s.gravatar.com/avatar/31d9e3c2369e37fead998201d7057fbe?d=mm" alt="Mohammad Haque">
                  <label class="comments__block-box__meta-tag comments__block-box__meta-tag-instructor label label-default">
                  Instructor
                  </label>
               </div>
               <div class="comments__block-box new-comment-form">
                  <div class="comment-arrow-border">
                     <div class="comment-arrow"></div>
                  </div>
                  <div class="comments__block-box__meta">
                     <div class="comments__block-box__meta-name">Mohammad Haque</div>
                     <div class="comments__block-box__meta-now">now</div>
                  </div>
                  <form action="/api/v1/comments" class="new-comment-form" method="post" target="_blank" rel="noopener" data-type="json" data-remote="true" id="new_comment_form" data-comment-handler="create_comment">
                     <div class="alert alert-success comments__block-box__alert--posted" id="posted_msg">
                        Your comment was posted.
                     </div>
                     <div class="alert alert-warning comments__block-box__alert--review" id="review_msg">
                        <i class="fa fa-check"></i> Your comment was posted, but it needs to be approved by the school owner before it shows up.
                     </div>
                     <div class="alert alert-danger comments__block-box__alert--failed" id="failed_msg">
                        Sorry, your comment could not be posted at this time :(
                     </div>
                     <div class="alert alert-danger text-error" id="text_error_alert">
                        Please add text to the comment.
                     </div>
                     <input type="hidden" name="comments[commentable_type]" value="Attachment">
                     <input type="hidden" name="comments[commentable_id]" value="39345934">
                     <div class="comment-editor">
                        <textarea aria-label="Comment box" placeholder="Leave a comment..." name="comments[body]" id="comment_body"></textarea>
                        <div class="new attachments-editor" id="attachments_editor">
                           <a href="#" class="add-attachment" id="add_attachment" data-comment-handler="add_image">
                           <img src="https://fedora.teachablecdn.com/assets/icon-image-add-2bb59d5f21dbca28cca9cf9e6530fc84f468b86806ca92da9e2bdafa2aa398d0.svg" alt="Add Image">
                           </a>
                           <ul class="attachments" data-editable-attachments="true"></ul>
                        </div>
                     </div>
                     <input class="btn btn-primary pull-right btn-md" type="submit" value="Post Comment">
                  </form>
               </div>
            </div>
         </div>
         <h4 class="comments__heading">
            <span id="comments_total">2</span> comments
         </h4>
         <div class="comments__wrapper" id="comments_wrapper">
            <div class="comments__block comments__block--student comments__block--indent-level-0 comments__block--approved" id="comment_11288712">
               <div class="commenter-profile">
                  <img class="gravatar" src="https://s.gravatar.com/avatar/ebdcda5b1f91229f701577932d26f502?d=mm&amp;s=70" alt="Ibrahim Atallah">
               </div>
               <div class="comments__block-box">
                  <div class="comment-arrow-border">
                     <div class="comment-arrow"></div>
                  </div>
                  <div class="comments__block-box__meta">
                     <div class="comments__block-box__meta-name">Ibrahim Atallah</div>
                     <label class="comments__block-box__meta-tag comments__block-box__meta-tag-review label label-warning">
                     Awaiting Review
                     </label>
                     <div class="comments__block-box__meta-time">
                        a year ago
                     </div>
                     <div class="comments__block-box__meta-actions">
                        <a href="javascript:void(0)" class="comments__block-box__meta-actions-reply" id="reply_btn" data-comment-id="11288712" data-comment-handler="add_reply">
                           <i class="fa fa-reply" aria-hidden="true"></i>
                           <div class="comment-action-tooltip">
                              Reply
                           </div>
                        </a>
                        <a href="https://learn.drlesliekorn.com/courses/856463/lectures/18234735/comments/11288712" target="_blank" rel="noopener" class="comments__block-box__meta-actions-permalink" id="permalink_11288712">
                           <i class="fa fa-link" aria-hidden="true"></i>
                           <div class="comment-action-tooltip">
                              Link
                           </div>
                        </a>
                     </div>
                  </div>
                  <div class="comment-content">
                     <div data-original-comment-body="Dr. Korn,
                        I am excited to take this course. Previously, I completed the Certified Mental Health Integrative Medicine Provider (CMHIMP) Training Course and looking forward to take the advanced coourse. With this course however, , I am not able to access the handouts. Please advice. 
                        Thank You,
                        Ibrahim," class="comments__block-box__body">Dr. Korn,
                        I am excited to take this course. Previously, I completed the Certified Mental Health Integrative Medicine Provider (CMHIMP) Training Course and looking forward to take the advanced coourse. With this course however, , I am not able to access the handouts. Please advice. 
                        Thank You,
                        Ibrahim,
                     </div>
                  </div>
               </div>
            </div>
            <div class="comments__block comments__block--student comments__block--indent-level-1 comments__block--current-user comments__block--new hidden" id="comment_11288712_reply_container">
               <div class="commenter-profile">
                  <img class="gravatar" src="https://s.gravatar.com/avatar/31d9e3c2369e37fead998201d7057fbe?d=mm" alt="Mohammad Haque">
               </div>
               <div class="comments__block-box new-comment-form">
                  <div class="comment-arrow-border">
                     <div class="comment-arrow"></div>
                  </div>
                  <div class="comments__block-box__meta">
                     <div class="comments__block-box__meta-name">Mohammad Haque</div>
                     <div class="comments__block-box__meta-now">now</div>
                  </div>
                  <form action="/api/v1/comments" class="new-comment-reply-form" id="comment_11288712_reply_form" data-parent-id="11288712" data-comment-handler="create_reply">
                     <div class="alert alert-success comments__block-box__alert--posted">
                        Your comment was posted.
                     </div>
                     <div class="alert alert-warning comments__block-box__alert--review">
                        <i class="fa fa-check" aria-hidden="true"></i> Your comment was posted, but it needs to be approved by the school owner before it shows up.
                     </div>
                     <div class="alert alert-danger comments__block-box__alert--failed">
                        Sorry, your comment could not be posted at this time :(
                     </div>
                     <div class="alert alert-danger text-error" id="text_error_alert">
                        Please add text to the comment.
                     </div>
                     <input type="hidden" name="comments[commentable_id]" value="39345934">
                     <input type="hidden" name="comments[commentable_type]" value="Attachment">
                     <input type="hidden" name="comments[parent_id]" value="11288712">
                     <div class="comment-editor">
                        <textarea placeholder="Leave a comment..." name="comments[body]" id="comment_body"></textarea>
                        <div class="attachments-editor" id="attachments_editor">
                           <a href="#" class="add-attachment" id="add_attachment" data-comment-handler="add_image">
                           <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjkiIGhlaWdodD0iMjciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSIjOTlBMEI4IiBzdHJva2Utd2lkdGg9IjEuOCIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiPjxwYXRoIGQ9Ik0yMy44IDE0Ljk0djcuODQxYTIuMzk1IDIuMzk1IDAgMDEtMi4zOTcgMi4zOTlIMy4zOTdBMi4zOTQgMi4zOTQgMCAwMTEgMjIuNzg0VjguMTk2QTIuNDAyIDIuNDAyIDAgMDEzLjM5MyA1LjhoMTEuOTczIi8+PHBhdGggZD0iTTEuOTEyIDE3LjYybDUuNTYtNS4xOTUgNi4wODEgNS45MzUgMy4xNzEtMy4wNDIgNS41NiA1LjU4NU0yMy44IDEuNzM1djguMjlNMjcuOTQ1IDUuODhoLTguMjkiLz48L2c+PC9zdmc+Cg==" alt="Add Image">
                           </a>
                           <ul class="attachments" data-editable-attachments="true"></ul>
                        </div>
                     </div>
                     <input class="btn btn-primary pull-right btn-md" type="submit" value="Post Comment">
                  </form>
               </div>
            </div>
            <div class="comments__block comments__block--owner comments__block--indent-level-1 comments__block--approved" id="comment_11292977">
               <div class="commenter-profile">
                  <img class="gravatar" src="https://s.gravatar.com/avatar/b6c559cf285096eed003c5e15e44b410?d=mm&amp;s=70" alt="Angel Estrada">
                  <label class="comments__block-box__meta-tag comments__block-box__meta-tag-instructor label label-default">
                  Instructor
                  </label>
               </div>
               <div class="comments__block-box">
                  <div class="comment-arrow-border">
                     <div class="comment-arrow"></div>
                  </div>
                  <div class="comments__block-box__meta">
                     <div class="comments__block-box__meta-name">Angel Estrada</div>
                     <label class="comments__block-box__meta-tag comments__block-box__meta-tag-review label label-warning">
                     Awaiting Review
                     </label>
                     <div class="comments__block-box__meta-time">
                        a year ago
                     </div>
                     <div class="comments__block-box__meta-actions">
                        <a href="javascript:void(0)" class="comments__block-box__meta-actions-reply" id="reply_btn" data-comment-id="11292977" data-comment-handler="add_reply">
                           <i class="fa fa-reply" aria-hidden="true"></i>
                           <div class="comment-action-tooltip">
                              Reply
                           </div>
                        </a>
                        <a href="https://learn.drlesliekorn.com/courses/856463/lectures/18234735/comments/11292977" target="_blank" rel="noopener" class="comments__block-box__meta-actions-permalink" id="permalink_11292977">
                           <i class="fa fa-link" aria-hidden="true"></i>
                           <div class="comment-action-tooltip">
                              Link
                           </div>
                        </a>
                     </div>
                  </div>
                  <div class="comment-content">
                     <div data-original-comment-body="Hi, Ibrahim
                        Hope this email finds you well. About your question, you can download the handouts under each module. This means that you will have to advance in the lessons in order to access the next handouts.
                        To download you have to click on the name of each handout and the process will start.
                        If you have any other question, please let me know.
                        --
                        Angel Estrada
                        Support Team" class="comments__block-box__body">Hi, Ibrahim
                        Hope this email finds you well. About your question, you can download the handouts under each module. This means that you will have to advance in the lessons in order to access the next handouts.
                        To download you have to click on the name of each handout and the process will start.
                        If you have any other question, please let me know.
                        --
                        Angel Estrada
                        Support Team
                     </div>
                  </div>
               </div>
            </div>
            <div class="comments__block comments__block--student comments__block--indent-level-2 comments__block--current-user comments__block--new hidden" id="comment_11292977_reply_container">
               <div class="commenter-profile">
                  <img class="gravatar" src="https://s.gravatar.com/avatar/31d9e3c2369e37fead998201d7057fbe?d=mm" alt="Mohammad Haque">
               </div>
               <div class="comments__block-box new-comment-form">
                  <div class="comment-arrow-border">
                     <div class="comment-arrow"></div>
                  </div>
                  <div class="comments__block-box__meta">
                     <div class="comments__block-box__meta-name">Mohammad Haque</div>
                     <div class="comments__block-box__meta-now">now</div>
                  </div>
                  <form action="/api/v1/comments" class="new-comment-reply-form" id="comment_11292977_reply_form" data-parent-id="11292977" data-comment-handler="create_reply">
                     <div class="alert alert-success comments__block-box__alert--posted">
                        Your comment was posted.
                     </div>
                     <div class="alert alert-warning comments__block-box__alert--review">
                        <i class="fa fa-check" aria-hidden="true"></i> Your comment was posted, but it needs to be approved by the school owner before it shows up.
                     </div>
                     <div class="alert alert-danger comments__block-box__alert--failed">
                        Sorry, your comment could not be posted at this time :(
                     </div>
                     <div class="alert alert-danger text-error" id="text_error_alert">
                        Please add text to the comment.
                     </div>
                     <input type="hidden" name="comments[commentable_id]" value="39345934">
                     <input type="hidden" name="comments[commentable_type]" value="Attachment">
                     <input type="hidden" name="comments[parent_id]" value="11292977">
                     <div class="comment-editor">
                        <textarea placeholder="Leave a comment..." name="comments[body]" id="comment_body"></textarea>
                        <div class="attachments-editor" id="attachments_editor">
                           <a href="#" class="add-attachment" id="add_attachment" data-comment-handler="add_image">
                           <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjkiIGhlaWdodD0iMjciIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSIjOTlBMEI4IiBzdHJva2Utd2lkdGg9IjEuOCIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiPjxwYXRoIGQ9Ik0yMy44IDE0Ljk0djcuODQxYTIuMzk1IDIuMzk1IDAgMDEtMi4zOTcgMi4zOTlIMy4zOTdBMi4zOTQgMi4zOTQgMCAwMTEgMjIuNzg0VjguMTk2QTIuNDAyIDIuNDAyIDAgMDEzLjM5MyA1LjhoMTEuOTczIi8+PHBhdGggZD0iTTEuOTEyIDE3LjYybDUuNTYtNS4xOTUgNi4wODEgNS45MzUgMy4xNzEtMy4wNDIgNS41NiA1LjU4NU0yMy44IDEuNzM1djguMjlNMjcuOTQ1IDUuODhoLTguMjkiLz48L2c+PC9zdmc+Cg==" alt="Add Image">
                           </a>
                           <ul class="attachments" data-editable-attachments="true"></ul>
                        </div>
                     </div>
                     <input class="btn btn-primary pull-right btn-md" type="submit" value="Post Comment">
                  </form>
               </div>
            </div>
         </div>
         <div class="comments__heading">
            <a class="comments-pagination-link hidden" id="comments_pagination" data-comment-handler="load_more">
            Load more
            </a>
         </div>
      </div>
   </div>
   <div id="empty_box"></div>
   <!-- Scroll to current lecture link position in sidebar -->
</div>
@endsection