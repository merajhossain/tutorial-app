@extends('courseCurriculumLayout')
@section('content')
<style>
   html, body{
      overflow: hidden;
   }
   </style>
<div class="course-mainbar" style="display: block;">
   <h2 class="section-title p-0">
      Course Instructor
   </h2>
   <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 text-center">
         <img class="mr-3 img-fluid rounded-circle" src="{{ asset('assets/images/m-haque-large.jpg') }}"  style="height: 250px;width: 250px;">
        <div class="author-name mt-3 font-size-h4">
         <strong>Dr. M. Haque</strong>
        </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-8">
         <p class="text-content"><span style="font-size: 28px; " class="mb-3 d-block">Functional Regenerative Medicine And Holistic Health Practitioner</span>
            Dr. M. Haque is a Certified Functional Diagnostic and Integrative Medicine Consultant. In addition he is Doctor of Naturopathy (ND) and licensed Holistic Health Practitioner. Before getting certified, he earned his Doctorate (PhD) degree in Bio-organic Chemistry from The University of Wisconsin Milwaukee and Postdoctoral Research in Medicine within The University of Texas Medical Branch. He studied Nutrition in health and wellbeing at the Harvard University, Massachusetts, USA. He wrote over hundred articles on health and wellbeing. Dr. Haque is also a Professor and Vice-President of University of Integrated Health (UIH), USA.</p>
      </div>
    </div>
</div>
@endsection