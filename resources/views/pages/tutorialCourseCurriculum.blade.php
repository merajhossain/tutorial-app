@extends('courseCurriculumLayout')
@section('content')
<style>
   html, body{
      overflow: hidden;
   }
   </style>
<div class="course-mainbar" style="display: block;">
    <!-- Meta tag for tracking lecture progress -->
    <meta id="lecture-completion-data" data-last-lecture-id="19207135" data-last-lecture-url="/courses/856463/lectures/19207135">
    <h2 class="section-title p-0">
       Course Curriculum
    </h2>
    <div class="next-lecture-wrapper">
       <a class="btn btn-primary start-next-lecture" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Start Next" data-ss-event-type="button" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/856463/lectures/19207135">
          Start next lecture
          <svg width="24" height="24" class="icon-m v-middle m-l-1-xs">
             <use xlink:href="#icon__ArrowForward"></use>
          </svg>
       </a>
       <span class="next-lecture-name hidden-sm">
       Culture Part 2
       (29:14)
       </span>
    </div>
    <!-- Lecture list on courses page (enrolled user) -->
    <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Welcome
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                        <i class="fas fa-align-justify"></i>
                      </span>
                      <span class="lecture-name">
                      Study Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div>
    <div class="row">
        <div class="col-sm-12 course-section">
           <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
              <span class="section-lock v-middle">
                 <svg width="24" height="24">
                    <use xlink:href="#icon__LockClock"></use>
                 </svg>
                 &nbsp;
              </span>
              Module 6 - Culture, Illness and Awareness
              <div class="section-days-to-drip">
                 <div class="section-days-logged-in">
                    Available in
                    <span class="section-days-to-drip-number"></span>
                    days
                 </div>
                 <div class="section-days-logged-out">
                    <span class="section-days-to-drip-number"></span>
                    days
                    after you enroll
                 </div>
              </div>
           </div>
           <ul class="section-list">
              <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                 <a class="item" href="/course-lecture">
                    <span class="status-container" aria-label="Completed item">
                    <span class="status-icon">
                    &nbsp;
                    </span>
                    </span>
                    <div class="title-container">
                       <div class="btn-primary btn-sm pull-right">
                          Start
                       </div>
                       <span class="lecture-icon v-middle">
                            <i class="fas fa-video"></i>
                       </span>
                       <span class="lecture-name">
                       Culture Part 1
                       (28:31)
                       </span>
                    </div>
                 </a>
              </li>
              <li class="section-item incomplete" data-lecture-id="17967424">
                 <a class="item" href="/course-lecture">
                    <span class="status-container">
                    <span class="status-icon">
                    &nbsp;
                    </span>
                    </span>
                    <div class="title-container">
                       <div class="btn-primary btn-sm pull-right">
                          Start
                       </div>
                       <span class="lecture-icon v-middle">
                        <i class="fas fa-video"></i>
                       </span>
                       <span class="lecture-name">
                       Culture Part 3
                       (14:12)
                       </span>
                    </div>
                 </a>
              </li>
              <li class="section-item incomplete" data-lecture-id="17967427">
                 <a class="item" href="/course-lecture">
                    <span class="status-container">
                    <span class="status-icon">
                    &nbsp;
                    </span>
                    </span>
                    <div class="title-container">
                       <div class="btn-primary btn-sm pull-right">
                          Start
                       </div>
                       <span class="lecture-icon v-middle">
                        <i class="fas fa-video"></i>
                       </span>
                       <span class="lecture-name">
                       Culture Part 4
                       (4:40)
                       </span>
                    </div>
                 </a>
              </li>
              <li class="section-item incomplete" data-lecture-id="17967435">
                 <a class="item" href="/course-lecture">
                    <span class="status-container">
                    <span class="status-icon">
                    &nbsp;
                    </span>
                    </span>
                    <div class="title-container">
                       <div class="btn-primary btn-sm pull-right">
                          Start
                       </div>
                       <span class="lecture-icon v-middle">
                        <i class="fas fa-video"></i>
                       </span>
                       <span class="lecture-name">
                       Culture Part 5
                       (1:21)
                       </span>
                    </div>
                 </a>
              </li>
              <li class="section-item incomplete" data-lecture-id="20870579">
                 <a class="item" href="/course-lecture">
                    <span class="status-container">
                    <span class="status-icon">
                    &nbsp;
                    </span>
                    </span>
                    <div class="title-container">
                       <div class="btn-primary btn-sm pull-right">
                          Start
                       </div>
                       <span class="lecture-icon v-middle">
                        <i class="fas fa-video"></i>
                       </span>
                       <span class="lecture-name">
                       Quiz
                       </span>
                    </div>
                 </a>
              </li>
           </ul>
        </div>
     </div>
     <div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div><div class="row">
      <div class="col-sm-12 course-section">
         <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
            <span class="section-lock v-middle">
               <svg width="24" height="24">
                  <use xlink:href="#icon__LockClock"></use>
               </svg>
               &nbsp;
            </span>
            Welcome
            <div class="section-days-to-drip">
               <div class="section-days-logged-in">
                  Available in
                  <span class="section-days-to-drip-number"></span>
                  days
               </div>
               <div class="section-days-logged-out">
                  <span class="section-days-to-drip-number"></span>
                  days
                  after you enroll
               </div>
            </div>
         </div>
         <ul class="section-list">
            <li class="section-item completed unlocked-lecture" data-lecture-id="20025579">
               <a class="item" href="/course-lecture">
                  <span class="status-container" aria-label="Completed item">
                  <span class="status-icon">
                  &nbsp;
                  </span>
                  </span>
                  <div class="title-container">
                     <div class="btn-primary btn-sm pull-right">
                        Start
                     </div>
                     <span class="lecture-icon v-middle">
                       <i class="fas fa-align-justify"></i>
                     </span>
                     <span class="lecture-name">
                     Study Quiz
                     </span>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="row">
       <div class="col-sm-12 course-section">
          <div class="section-title px-0" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
             <span class="section-lock v-middle">
                <svg width="24" height="24">
                   <use xlink:href="#icon__LockClock"></use>
                </svg>
                &nbsp;
             </span>
             Module 6 - Culture, Illness and Awareness
             <div class="section-days-to-drip">
                <div class="section-days-logged-in">
                   Available in
                   <span class="section-days-to-drip-number"></span>
                   days
                </div>
                <div class="section-days-logged-out">
                   <span class="section-days-to-drip-number"></span>
                   days
                   after you enroll
                </div>
             </div>
          </div>
          <ul class="section-list">
             <li class="section-item completed unlocked-lecture" data-lecture-id="17967433">
                <a class="item" href="/course-lecture">
                   <span class="status-container" aria-label="Completed item">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                           <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 1
                      (28:31)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967424">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 3
                      (14:12)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967427">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 4
                      (4:40)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="17967435">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Culture Part 5
                      (1:21)
                      </span>
                   </div>
                </a>
             </li>
             <li class="section-item incomplete" data-lecture-id="20870579">
                <a class="item" href="/course-lecture">
                   <span class="status-container">
                   <span class="status-icon">
                   &nbsp;
                   </span>
                   </span>
                   <div class="title-container">
                      <div class="btn-primary btn-sm pull-right">
                         Start
                      </div>
                      <span class="lecture-icon v-middle">
                       <i class="fas fa-video"></i>
                      </span>
                      <span class="lecture-name">
                      Quiz
                      </span>
                   </div>
                </a>
             </li>
          </ul>
       </div>
    </div>
 </div>
@endsection