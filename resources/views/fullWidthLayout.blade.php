<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
        @include('partials.headerScript')
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-mobile-fixed full-width-footer">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<div class="container-fluid">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<!--begin::Header-->
										<nav class="navbar navbar-expand-lg navbar-light custom-nav py-0">
											<a class="navbar-brand" href="#">
												<img src="{{ asset('assets/images/image_2022_04_15T03_36_43_247Z.png') }}" class="max-h-50px" alt="">
											</a>
											<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
											<span class="navbar-toggler-icon"></span>
											</button>
											<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
											<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
												<li class="nav-item active">
													<a class="nav-link" href="#">My Courses</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" href="#">All Courses</a>
												</li>
												<li class="nav-item dropdown">
													<div class="">
														<a href="#" class="btn pt-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img class="gravatar" src="https://lh3.googleusercontent.com/-JM2xsdjz2Bw/AAAAAAAAAAI/AAAAAAAAAAA/DVECr-jVlk4/photo.jpg" alt="mojib111@gmail.com">
														</a>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<a class="dropdown-item" href="#">Edit Profile</a>
															<a class="dropdown-item" href="#">Manage Subscriptions</a>
															<a class="dropdown-item" href="#">Add / Change Credit Card</a>
															<a class="dropdown-item" href="#">Address</a>
															<a class="dropdown-item" href="#">Contact</a>
															<a class="dropdown-item" href="/sign_out">Log Out</a>
														</div>
													  </div>
												</li>
											</ul>
											</div>
										</nav>
										<!--end::Header-->
								</div>
							</div>
						</div>
					</div>
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid pt-0" id="kt_content">
						<div class="container-fluid">
							<div class="container">
								<!-- Start Content-->
								@yield('content')
								<!-- End Content-->
							</div>
						</div>
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					@include('partials.footer')
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		@include('partials.footerScript')
</html>