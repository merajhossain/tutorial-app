<div class="lecture-nav d-flex">
    <a class="nav-btn" href="" role="button" id="lecture_previous_button">
        <i class="fas fa-chevron-left"></i>
        &nbsp;&nbsp;
        <span class="nav-text">Previous Lecture</span>
    </a>
    <a class="nav-btn complete" data-cpl-tooltip="You must complete all lecture material before progressing." data-vpl-tooltip="90% of each video must be completed. You have completed %{progress_completed} of the current video." href="" id="lecture_complete_button" role="button">
      <span class="nav-text">Complete and Continue</span>
      &nbsp;&nbsp;
      <i class="fas fa-angle-right"></i>
    </a>
  </div>