<!--begin::Header Menu-->
<div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
    <!--begin::Header Nav-->
    <ul class="menu-nav ">
        <li class="menu-item">
            <a href="/tutorial-list" class="menu-link p-0 bg-none"><i class="fas fa-home fa-2x"></i></a>
        </li>
        <li class="menu-item">
            <div class="dropdown settings-dropdown" role="menubar">
                <a href="#" class="nav-icon-settings dropdown-toggle nav-focus" aria-label="Settings Menu" aria-haspopup="true" role="menuitem" id="settings_menu" data-toggle="dropdown">
                    <i class="fas fa-cog fa-2x"></i>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="settings_menu">
                   <!-- AUTOPLAY -->
                   <li class="nav-focus" aria-label="menuitem">
                      <div class="switch" id="switch-autoplay-lectures">
                         <input id="custom-toggle-autoplay" class="custom-toggle custom-toggle-round" name="toggle-autoplay" type="checkbox" aria-label="Autoplay" data-control-initialized="true">
                         <label for="custom-toggle-autoplay"></label>
                      </div>
                      <span aria-labelledby="switch-autoplay-lectures">Autoplay</span>
                   </li>
                   <!-- AUTOCOMPLETE -->
                   <li class="nav-focus" aria-label="menuitem">
                      <div class="switch" id="switch-autocomplete-lectures">
                         <input id="custom-toggle-autocomplete" class="custom-toggle custom-toggle-round" name="toggle-autocomplete" type="checkbox" aria-label="Autocomplete" data-control-initialized="true">
                         <label for="custom-toggle-autocomplete"></label>
                      </div>
                      <span aria-labelledby="switch-autocomplete-lectures">Autocomplete</span>
                   </li>
                   <!-- PLAYBACK SPEED -->
                   <li class="nav-focus" aria-label="menuitem">
                      <div class="pull-right">
                         <button class="playback-speed" role="button" data-control-initialized="true">
                         <span class="glyphicon glyphicon-forward"></span><span>1x</span></button>
                      </div>
                      Speed
                   </li>
                </ul>
             </div>
        </li>
    </ul>
    <!--end::Header Nav-->
</div>

