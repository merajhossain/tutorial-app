<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Brand-->
    <div class="brand flex-column-auto" id="kt_brand">
        <!--begin::Logo-->
        @include('partials.lecturesSidebarLogo')
        <!--end::Logo-->
        <!--begin::Toggle-->
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
            <span class="svg-icon svg-icon svg-icon-xl">
            </span>
        </button>
        <!--end::Toolbar-->
    </div>
    <!--end::Brand-->
    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <!--begin::Menu Container-->
        <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <div role="navigation" class="course-sidebar lecture-page navbar-collapse navbar-sidebar-collapse" id="courseSidebar" aria-expanded="false">
                <div class="course-sidebar-head">
                  <h2>Nutrition &amp; Integrative Medicine for Diabetes, Cognitive Decline &amp; Alzheimer’s Disease</h2>
                  <!-- Course Progress -->
                  
                  <div class="course-progress lecture-page is-at-top">
                    <div class="progressbar">
                      <div class="progressbar-fill" style="min-width: 23%;"></div>
                    </div>
                    <div class="small course-progress-percentage">
                      <span class="percentage">
                        23%
                      </span>
                      COMPLETE
                    </div>
                  </div>
                  
                </div>
                <!-- Lecture list on courses page (enrolled user) -->
              
                <div class="row lecture-sidebar">
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Welcome
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="20025579" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20025579" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20025579" data-ss-event-type="link" data-ss-lecture-id="20025579" data-ss-position="1" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20025579" id="sidebar_link_20025579">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Study Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 1 - Introduction to Integrative Medicine and Nutrition
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18234735" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234735" class="section-item completed unlocked-lecture next-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234735" data-ss-event-type="link" data-ss-lecture-id="18234735" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234735" id="sidebar_link_18234735">
                          <span class="status-container" aria-label="Current item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 1
                              
                              (18:32)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234740" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234740" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234740" data-ss-event-type="link" data-ss-lecture-id="18234740" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234740" id="sidebar_link_18234740">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 2
                              
                              (10:22)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234739" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234739" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234739" data-ss-event-type="link" data-ss-lecture-id="18234739" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234739" id="sidebar_link_18234739">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 3
                              
                              (6:10)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234738" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234738" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234738" data-ss-event-type="link" data-ss-lecture-id="18234738" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234738" id="sidebar_link_18234738">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 4
                              
                              (10:18)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234736" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234736" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234736" data-ss-event-type="link" data-ss-lecture-id="18234736" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234736" id="sidebar_link_18234736">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 5
                              
                              (9:48)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234734" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234734" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234734" data-ss-event-type="link" data-ss-lecture-id="18234734" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234734" id="sidebar_link_18234734">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Introduction Part 6
                              
                              (13:17)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20024897" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024897" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024897" data-ss-event-type="link" data-ss-lecture-id="20024897" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024897" id="sidebar_link_20024897">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20666985" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20666985" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20666985" data-ss-event-type="link" data-ss-lecture-id="20666985" data-ss-position="2" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20666985" id="sidebar_link_20666985">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 2 - Ethics and Scope of practice
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="17885676" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885676" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885676" data-ss-event-type="link" data-ss-lecture-id="17885676" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885676" id="sidebar_link_17885676">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Ethics Part 1
                              
                              (11:28)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885685" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885685" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885685" data-ss-event-type="link" data-ss-lecture-id="17885685" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885685" id="sidebar_link_17885685">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Ethics Part 2
                              
                              (10:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885681" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885681" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885681" data-ss-event-type="link" data-ss-lecture-id="17885681" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885681" id="sidebar_link_17885681">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Ethics Part 3
                              
                              (16:49)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885686" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885686" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885686" data-ss-event-type="link" data-ss-lecture-id="17885686" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885686" id="sidebar_link_17885686">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Ethics Part 4
                              
                              (4:03)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20024932" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024932" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024932" data-ss-event-type="link" data-ss-lecture-id="20024932" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20024932" id="sidebar_link_20024932">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20840839" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20840839" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20840839" data-ss-event-type="link" data-ss-lecture-id="20840839" data-ss-position="3" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20840839" id="sidebar_link_20840839">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 3 - The Biology of Diabetes and Alzheimer's
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="17885677" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885677" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885677" data-ss-event-type="link" data-ss-lecture-id="17885677" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885677" id="sidebar_link_17885677">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 1
                              
                              (20:21)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885678" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885678" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885678" data-ss-event-type="link" data-ss-lecture-id="17885678" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885678" id="sidebar_link_17885678">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 2
                              
                              (16:22)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885680" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885680" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885680" data-ss-event-type="link" data-ss-lecture-id="17885680" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885680" id="sidebar_link_17885680">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 3
                              
                              (31:18)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17902611" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17902611" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17902611" data-ss-event-type="link" data-ss-lecture-id="17902611" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17902611" id="sidebar_link_17902611">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 4
                              
                              (17:04)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885683" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885683" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885683" data-ss-event-type="link" data-ss-lecture-id="17885683" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885683" id="sidebar_link_17885683">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 5
                              
                              (29:19)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885682" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885682" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885682" data-ss-event-type="link" data-ss-lecture-id="17885682" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885682" id="sidebar_link_17885682">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Biology Part 6
                              
                              (1:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20856947" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20856947" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20856947" data-ss-event-type="link" data-ss-lecture-id="20856947" data-ss-position="4" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20856947" id="sidebar_link_20856947">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 4 - The Psychology of Diabetes and Alzheimer's
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="17885684" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885684" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885684" data-ss-event-type="link" data-ss-lecture-id="17885684" data-ss-position="5" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885684" id="sidebar_link_17885684">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Psychology Part 1
                              
                              (29:38)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885679" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885679" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885679" data-ss-event-type="link" data-ss-lecture-id="17885679" data-ss-position="5" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885679" id="sidebar_link_17885679">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Psychology Part 2
                              
                              (5:18)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17885675" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885675" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885675" data-ss-event-type="link" data-ss-lecture-id="17885675" data-ss-position="5" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17885675" id="sidebar_link_17885675">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Psychology Part 3
                              
                              (22:30)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20027970" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20027970" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20027970" data-ss-event-type="link" data-ss-lecture-id="20027970" data-ss-position="5" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20027970" id="sidebar_link_20027970">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20860273" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20860273" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20860273" data-ss-event-type="link" data-ss-lecture-id="20860273" data-ss-position="5" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20860273" id="sidebar_link_20860273">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 5 - Circadian Rhythms and Hormones
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="19267519" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19267519" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19267519" data-ss-event-type="link" data-ss-lecture-id="19267519" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19267519" id="sidebar_link_19267519">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Circadian Rhythms &amp; Hormones Part 1
                              
                              (30:00)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967431" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967431" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967431" data-ss-event-type="link" data-ss-lecture-id="17967431" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967431" id="sidebar_link_17967431">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Circadian Rhythms &amp; Hormones Part 2
                              
                              (10:12)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967426" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967426" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967426" data-ss-event-type="link" data-ss-lecture-id="17967426" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967426" id="sidebar_link_17967426">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Circadian Rhythms &amp; Hormones Part 3
                              
                              (3:57)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967432" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967432" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967432" data-ss-event-type="link" data-ss-lecture-id="17967432" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967432" id="sidebar_link_17967432">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Circadian Rhythms &amp; Hormones Part 4
                              
                              (14:12)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967428" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967428" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967428" data-ss-event-type="link" data-ss-lecture-id="17967428" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967428" id="sidebar_link_17967428">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Circadian Rhythms &amp; Hormones Part 5
                              
                              (11:38)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20028653" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028653" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028653" data-ss-event-type="link" data-ss-lecture-id="20028653" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028653" id="sidebar_link_20028653">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20861895" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20861895" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20861895" data-ss-event-type="link" data-ss-lecture-id="20861895" data-ss-position="6" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20861895" id="sidebar_link_20861895">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 6 - Culture, Illness and Awareness
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="17967433" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967433" class="section-item completed unlocked-lecture">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967433" data-ss-event-type="link" data-ss-lecture-id="17967433" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967433" id="sidebar_link_17967433">
                          <span class="status-container" aria-label="Completed item">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culture Part 1
                              
                              (28:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="19207135" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19207135" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19207135" data-ss-event-type="link" data-ss-lecture-id="19207135" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19207135" id="sidebar_link_19207135">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culture Part 2
                              
                              (29:14)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967424" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967424" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967424" data-ss-event-type="link" data-ss-lecture-id="17967424" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967424" id="sidebar_link_17967424">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culture Part 3
                              
                              (14:12)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967427" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967427" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967427" data-ss-event-type="link" data-ss-lecture-id="17967427" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967427" id="sidebar_link_17967427">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culture Part 4
                              
                              (4:40)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967435" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967435" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967435" data-ss-event-type="link" data-ss-lecture-id="17967435" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967435" id="sidebar_link_17967435">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culture Part 5
                              
                              (1:21)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20870579" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20870579" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20870579" data-ss-event-type="link" data-ss-lecture-id="20870579" data-ss-position="7" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20870579" id="sidebar_link_20870579">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 7 - Assessment and Evaluation
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="17967425" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967425" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967425" data-ss-event-type="link" data-ss-lecture-id="17967425" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967425" id="sidebar_link_17967425">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 1
                              
                              (2:22)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="19225572" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19225572" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19225572" data-ss-event-type="link" data-ss-lecture-id="19225572" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/19225572" id="sidebar_link_19225572">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 2
                              
                              (34:11)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967429" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967429" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967429" data-ss-event-type="link" data-ss-lecture-id="17967429" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967429" id="sidebar_link_17967429">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 3
                              
                              (6:19)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967430" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967430" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967430" data-ss-event-type="link" data-ss-lecture-id="17967430" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967430" id="sidebar_link_17967430">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 4
                              
                              (7:50)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967423" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967423" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967423" data-ss-event-type="link" data-ss-lecture-id="17967423" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967423" id="sidebar_link_17967423">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 5
                              
                              (2:20)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="17967434" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967434" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967434" data-ss-event-type="link" data-ss-lecture-id="17967434" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/17967434" id="sidebar_link_17967434">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 6
                              
                              (8:43)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183358" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183358" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183358" data-ss-event-type="link" data-ss-lecture-id="18183358" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183358" id="sidebar_link_18183358">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 7
                              
                              (13:28)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183356" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183356" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183356" data-ss-event-type="link" data-ss-lecture-id="18183356" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183356" id="sidebar_link_18183356">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 8
                              
                              (5:30)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183355" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183355" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183355" data-ss-event-type="link" data-ss-lecture-id="18183355" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183355" id="sidebar_link_18183355">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 9
                              
                              (4:15)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183347" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183347" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183347" data-ss-event-type="link" data-ss-lecture-id="18183347" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183347" id="sidebar_link_18183347">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Assessment Part 10
                              
                              (7:51)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20028925" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028925" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028925" data-ss-event-type="link" data-ss-lecture-id="20028925" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20028925" id="sidebar_link_20028925">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20871772" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20871772" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20871772" data-ss-event-type="link" data-ss-lecture-id="20871772" data-ss-position="8" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20871772" id="sidebar_link_20871772">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 8 - Digestion and Diet
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="20655046" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20655046" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20655046" data-ss-event-type="link" data-ss-lecture-id="20655046" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20655046" id="sidebar_link_20655046">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 1
                              
                              (29:36)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183348" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183348" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183348" data-ss-event-type="link" data-ss-lecture-id="18183348" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183348" id="sidebar_link_18183348">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 2
                              
                              (4:55)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183351" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183351" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183351" data-ss-event-type="link" data-ss-lecture-id="18183351" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183351" id="sidebar_link_18183351">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 3
                              
                              (0:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183353" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183353" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183353" data-ss-event-type="link" data-ss-lecture-id="18183353" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183353" id="sidebar_link_18183353">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 4
                              
                              (4:49)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183350" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183350" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183350" data-ss-event-type="link" data-ss-lecture-id="18183350" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183350" id="sidebar_link_18183350">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 5
                              
                              (2:21)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183357" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183357" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183357" data-ss-event-type="link" data-ss-lecture-id="18183357" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183357" id="sidebar_link_18183357">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 6
                              
                              (29:18)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183352" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183352" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183352" data-ss-event-type="link" data-ss-lecture-id="18183352" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183352" id="sidebar_link_18183352">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 7
                              
                              (9:36)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183349" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183349" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183349" data-ss-event-type="link" data-ss-lecture-id="18183349" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183349" id="sidebar_link_18183349">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 8
                              
                              (5:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18183354" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183354" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183354" data-ss-event-type="link" data-ss-lecture-id="18183354" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18183354" id="sidebar_link_18183354">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Digestion Part 9
                              
                              (9:06)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20029510" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20029510" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20029510" data-ss-event-type="link" data-ss-lecture-id="20029510" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20029510" id="sidebar_link_20029510">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20872145" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20872145" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20872145" data-ss-event-type="link" data-ss-lecture-id="20872145" data-ss-position="9" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20872145" id="sidebar_link_20872145">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 9 - Culinary and Spice Medicine
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18468602" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468602" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468602" data-ss-event-type="link" data-ss-lecture-id="18468602" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468602" id="sidebar_link_18468602">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 1
                              
                              (5:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18468600" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468600" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468600" data-ss-event-type="link" data-ss-lecture-id="18468600" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468600" id="sidebar_link_18468600">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 2
                              
                              (15:55)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18468601" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468601" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468601" data-ss-event-type="link" data-ss-lecture-id="18468601" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468601" id="sidebar_link_18468601">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 3
                              
                              (16:26)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18468599" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468599" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468599" data-ss-event-type="link" data-ss-lecture-id="18468599" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468599" id="sidebar_link_18468599">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 4
                              
                              (6:23)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18468597" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468597" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468597" data-ss-event-type="link" data-ss-lecture-id="18468597" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468597" id="sidebar_link_18468597">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 5
                              
                              (16:49)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18468598" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468598" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468598" data-ss-event-type="link" data-ss-lecture-id="18468598" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18468598" id="sidebar_link_18468598">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 6
                              
                              (28:07)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="15545061" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545061" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545061" data-ss-event-type="link" data-ss-lecture-id="15545061" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545061" id="sidebar_link_15545061">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Culinary &amp; Spice Medicine Part 7
                              
                              (29:52)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20030642" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20030642" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20030642" data-ss-event-type="link" data-ss-lecture-id="20030642" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20030642" id="sidebar_link_20030642">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20873710" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20873710" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20873710" data-ss-event-type="link" data-ss-lecture-id="20873710" data-ss-position="10" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20873710" id="sidebar_link_20873710">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 10 - Herbal Medicine and Diabetes
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="16486584" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486584" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486584" data-ss-event-type="link" data-ss-lecture-id="16486584" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486584" id="sidebar_link_16486584">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 1
                              
                              (19:17)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486592" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486592" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486592" data-ss-event-type="link" data-ss-lecture-id="16486592" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486592" id="sidebar_link_16486592">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 2
                              
                              (4:53)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16551283" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16551283" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16551283" data-ss-event-type="link" data-ss-lecture-id="16551283" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16551283" id="sidebar_link_16551283">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 3
                              
                              (41:47)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486594" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486594" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486594" data-ss-event-type="link" data-ss-lecture-id="16486594" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486594" id="sidebar_link_16486594">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 4
                              
                              (12:01)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486575" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486575" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486575" data-ss-event-type="link" data-ss-lecture-id="16486575" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486575" id="sidebar_link_16486575">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 5
                              
                              (13:15)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486581" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486581" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486581" data-ss-event-type="link" data-ss-lecture-id="16486581" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486581" id="sidebar_link_16486581">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 6
                              
                              (12:50)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486572" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486572" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486572" data-ss-event-type="link" data-ss-lecture-id="16486572" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486572" id="sidebar_link_16486572">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 7
                              
                              (8:16)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16486589" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486589" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486589" data-ss-event-type="link" data-ss-lecture-id="16486589" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16486589" id="sidebar_link_16486589">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Herbal Medicine Part 8
                              
                              (47:03)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20031287" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031287" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031287" data-ss-event-type="link" data-ss-lecture-id="20031287" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031287" id="sidebar_link_20031287">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20886404" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20886404" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20886404" data-ss-event-type="link" data-ss-lecture-id="20886404" data-ss-position="11" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20886404" id="sidebar_link_20886404">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 11 - Nutritional Supplements
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="16780075" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16780075" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16780075" data-ss-event-type="link" data-ss-lecture-id="16780075" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16780075" id="sidebar_link_16780075">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 1
                              
                              (28:18)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564446" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564446" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564446" data-ss-event-type="link" data-ss-lecture-id="18564446" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564446" id="sidebar_link_18564446">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 2
                              
                              (20:50)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564439" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564439" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564439" data-ss-event-type="link" data-ss-lecture-id="18564439" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564439" id="sidebar_link_18564439">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 3
                              
                              (22:26)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564431" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564431" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564431" data-ss-event-type="link" data-ss-lecture-id="18564431" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564431" id="sidebar_link_18564431">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 4
                              
                              (13:25)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16757448" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16757448" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16757448" data-ss-event-type="link" data-ss-lecture-id="16757448" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16757448" id="sidebar_link_16757448">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 5
                              
                              (15:10)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564442" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564442" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564442" data-ss-event-type="link" data-ss-lecture-id="18564442" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564442" id="sidebar_link_18564442">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 6
                              
                              (12:44)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564444" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564444" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564444" data-ss-event-type="link" data-ss-lecture-id="18564444" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564444" id="sidebar_link_18564444">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 7
                              
                              (1:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564432" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564432" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564432" data-ss-event-type="link" data-ss-lecture-id="18564432" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564432" id="sidebar_link_18564432">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 8
                              
                              (14:00)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564437" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564437" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564437" data-ss-event-type="link" data-ss-lecture-id="18564437" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564437" id="sidebar_link_18564437">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Nutrition  Part 9
                              
                              (23:41)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20887072" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887072" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887072" data-ss-event-type="link" data-ss-lecture-id="20887072" data-ss-position="12" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887072" id="sidebar_link_20887072">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 12 - Pharmaceuticals and their Interactions with Herbal Supplements
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18234731" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234731" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234731" data-ss-event-type="link" data-ss-lecture-id="18234731" data-ss-position="13" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234731" id="sidebar_link_18234731">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Pharmaceuticals Part 1
                              
                              (27:47)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234737" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234737" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234737" data-ss-event-type="link" data-ss-lecture-id="18234737" data-ss-position="13" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234737" id="sidebar_link_18234737">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Pharmaceuticals Part 2
                              
                              (28:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234733" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234733" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234733" data-ss-event-type="link" data-ss-lecture-id="18234733" data-ss-position="13" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234733" id="sidebar_link_18234733">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Pharmaceuticals Part 3
                              
                              (5:42)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18234732" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234732" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234732" data-ss-event-type="link" data-ss-lecture-id="18234732" data-ss-position="13" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18234732" id="sidebar_link_18234732">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Pharmaceuticals Part 4
                              
                              (23:24)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20887900" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887900" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887900" data-ss-event-type="link" data-ss-lecture-id="20887900" data-ss-position="13" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20887900" id="sidebar_link_20887900">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 13 - Exercise and Movement for Diabetes and AD
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18564447" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564447" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564447" data-ss-event-type="link" data-ss-lecture-id="18564447" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564447" id="sidebar_link_18564447">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Exercise Part 1
                              
                              (18:24)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564433" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564433" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564433" data-ss-event-type="link" data-ss-lecture-id="18564433" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564433" id="sidebar_link_18564433">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Exercise Part 2
                              
                              (9:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564429" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564429" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564429" data-ss-event-type="link" data-ss-lecture-id="18564429" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564429" id="sidebar_link_18564429">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Exercise Part 3
                              
                              (32:36)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993020" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993020" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993020" data-ss-event-type="link" data-ss-lecture-id="16993020" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993020" id="sidebar_link_16993020">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises for Diabetes &amp; Brain Balance Part 1
                              
                              (5:00)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993029" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993029" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993029" data-ss-event-type="link" data-ss-lecture-id="16993029" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993029" id="sidebar_link_16993029">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 2
                              
                              (1:10)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993034" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993034" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993034" data-ss-event-type="link" data-ss-lecture-id="16993034" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993034" id="sidebar_link_16993034">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 3
                              
                              (2:57)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993025" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993025" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993025" data-ss-event-type="link" data-ss-lecture-id="16993025" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993025" id="sidebar_link_16993025">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 4
                              
                              (1:55)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993028" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993028" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993028" data-ss-event-type="link" data-ss-lecture-id="16993028" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993028" id="sidebar_link_16993028">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 5
                              
                              (4:54)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993030" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993030" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993030" data-ss-event-type="link" data-ss-lecture-id="16993030" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993030" id="sidebar_link_16993030">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 6
                              
                              (0:58)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993035" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993035" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993035" data-ss-event-type="link" data-ss-lecture-id="16993035" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993035" id="sidebar_link_16993035">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 7
                              
                              (1:44)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993027" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993027" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993027" data-ss-event-type="link" data-ss-lecture-id="16993027" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993027" id="sidebar_link_16993027">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 8
                              
                              (0:43)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993021" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993021" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993021" data-ss-event-type="link" data-ss-lecture-id="16993021" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993021" id="sidebar_link_16993021">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 9
                              
                              (0:33)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993026" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993026" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993026" data-ss-event-type="link" data-ss-lecture-id="16993026" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993026" id="sidebar_link_16993026">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 10
                              
                              (0:37)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993032" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993032" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993032" data-ss-event-type="link" data-ss-lecture-id="16993032" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993032" id="sidebar_link_16993032">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 11
                              
                              (0:43)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993023" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993023" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993023" data-ss-event-type="link" data-ss-lecture-id="16993023" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993023" id="sidebar_link_16993023">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 12
                              
                              (0:52)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993036" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993036" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993036" data-ss-event-type="link" data-ss-lecture-id="16993036" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993036" id="sidebar_link_16993036">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 13
                              
                              (2:09)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993024" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993024" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993024" data-ss-event-type="link" data-ss-lecture-id="16993024" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993024" id="sidebar_link_16993024">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 14
                              
                              (3:01)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993022" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993022" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993022" data-ss-event-type="link" data-ss-lecture-id="16993022" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993022" id="sidebar_link_16993022">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 15
                              
                              (12:30)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993031" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993031" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993031" data-ss-event-type="link" data-ss-lecture-id="16993031" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993031" id="sidebar_link_16993031">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 16
                              
                              (1:48)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16993033" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993033" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993033" data-ss-event-type="link" data-ss-lecture-id="16993033" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16993033" id="sidebar_link_16993033">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Simple Home Exercises Part 17
                              
                              (1:21)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20031907" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031907" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031907" data-ss-event-type="link" data-ss-lecture-id="20031907" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20031907" id="sidebar_link_20031907">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20892154" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892154" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892154" data-ss-event-type="link" data-ss-lecture-id="20892154" data-ss-position="14" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892154" id="sidebar_link_20892154">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 14 - Somatic and Energy Therapies
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18564449" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564449" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564449" data-ss-event-type="link" data-ss-lecture-id="18564449" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564449" id="sidebar_link_18564449">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Somatic Part 1
                              
                              (3:16)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564438" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564438" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564438" data-ss-event-type="link" data-ss-lecture-id="18564438" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564438" id="sidebar_link_18564438">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Somatic Part 2
                              
                              (3:21)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564440" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564440" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564440" data-ss-event-type="link" data-ss-lecture-id="18564440" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564440" id="sidebar_link_18564440">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Somatic Part 3
                              
                              (18:00)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564436" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564436" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564436" data-ss-event-type="link" data-ss-lecture-id="18564436" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564436" id="sidebar_link_18564436">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Somatic Part 4
                              
                              (2:26)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16970808" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16970808" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16970808" data-ss-event-type="link" data-ss-lecture-id="16970808" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16970808" id="sidebar_link_16970808">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Somatic Part 5
                              
                              (35:04)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962910" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962910" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962910" data-ss-event-type="link" data-ss-lecture-id="16962910" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962910" id="sidebar_link_16962910">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 1: Upper Body
                              
                              (23:53)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962911" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962911" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962911" data-ss-event-type="link" data-ss-lecture-id="16962911" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962911" id="sidebar_link_16962911">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 2: Abdominal Region
                              
                              (8:04)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962913" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962913" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962913" data-ss-event-type="link" data-ss-lecture-id="16962913" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962913" id="sidebar_link_16962913">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 3: Feet
                              
                              (13:28)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962915" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962915" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962915" data-ss-event-type="link" data-ss-lecture-id="16962915" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962915" id="sidebar_link_16962915">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 4: Legs
                              
                              (6:46)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962912" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962912" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962912" data-ss-event-type="link" data-ss-lecture-id="16962912" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962912" id="sidebar_link_16962912">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 5: Back
                              
                              (9:46)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962914" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962914" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962914" data-ss-event-type="link" data-ss-lecture-id="16962914" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962914" id="sidebar_link_16962914">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 6: Head &amp; Final Contacts
                              
                              (2:50)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="16962909" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962909" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962909" data-ss-event-type="link" data-ss-lecture-id="16962909" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/16962909" id="sidebar_link_16962909">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Massage and Bodywork for D&amp;A Part 7: Skin Brushing
                              
                              (4:08)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20032336" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20032336" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20032336" data-ss-event-type="link" data-ss-lecture-id="20032336" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20032336" id="sidebar_link_20032336">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20892664" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892664" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892664" data-ss-event-type="link" data-ss-lecture-id="20892664" data-ss-position="15" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20892664" id="sidebar_link_20892664">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 15 - Detoxification Strategies
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18564445" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564445" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564445" data-ss-event-type="link" data-ss-lecture-id="18564445" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564445" id="sidebar_link_18564445">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Detoxification Part 1
                              
                              (25:08)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564428" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564428" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564428" data-ss-event-type="link" data-ss-lecture-id="18564428" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564428" id="sidebar_link_18564428">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Detoxification Part 2
                              
                              (11:13)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564441" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564441" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564441" data-ss-event-type="link" data-ss-lecture-id="18564441" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564441" id="sidebar_link_18564441">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Detoxification Part 3
                              
                              (18:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564434" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564434" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564434" data-ss-event-type="link" data-ss-lecture-id="18564434" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564434" id="sidebar_link_18564434">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Detoxification Part 4
                              
                              (14:00)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20033380" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033380" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033380" data-ss-event-type="link" data-ss-lecture-id="20033380" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033380" id="sidebar_link_20033380">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20894311" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20894311" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20894311" data-ss-event-type="link" data-ss-lecture-id="20894311" data-ss-position="16" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20894311" id="sidebar_link_20894311">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 16 - Self Care, Self Regulation, Adherence
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18564443" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564443" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564443" data-ss-event-type="link" data-ss-lecture-id="18564443" data-ss-position="17" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564443" id="sidebar_link_18564443">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Self Care Part 1
                              
                              (28:28)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564430" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564430" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564430" data-ss-event-type="link" data-ss-lecture-id="18564430" data-ss-position="17" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564430" id="sidebar_link_18564430">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Self Care Part 2
                              
                              (10:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20033747" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033747" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033747" data-ss-event-type="link" data-ss-lecture-id="20033747" data-ss-position="17" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033747" id="sidebar_link_20033747">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="15545106" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545106" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545106" data-ss-event-type="link" data-ss-lecture-id="15545106" data-ss-position="17" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/15545106" id="sidebar_link_15545106">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 17 - Protocols &amp; Next Steps
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="18564435" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564435" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564435" data-ss-event-type="link" data-ss-lecture-id="18564435" data-ss-position="18" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564435" id="sidebar_link_18564435">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Protocols Part 1
                              
                              (8:35)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="18564448" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564448" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564448" data-ss-event-type="link" data-ss-lecture-id="18564448" data-ss-position="18" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/18564448" id="sidebar_link_18564448">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Video"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Protocols 2
                              
                              (40:31)
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="20986809" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20986809" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20986809" data-ss-event-type="link" data-ss-lecture-id="20986809" data-ss-position="18" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20986809" id="sidebar_link_20986809">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Quiz"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Quiz
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Module 18 - Resources
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="20033959" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033959" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033959" data-ss-event-type="link" data-ss-lecture-id="20033959" data-ss-position="19" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/20033959" id="sidebar_link_20033959">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Handouts
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                  <div class="col-sm-12 course-section">
                    <div role="heading" aria-level="3" class="section-title" data-release-date="" data-days-until-dripped="" data-is-dripped-by-date="" data-course-id="856463">
                      <span class="section-lock v-middle">
                        <svg width="24" height="24">
                          <use xlink:href="#icon__LockClock"></use>
                        </svg>&nbsp;
                      </span>
                      Get your Certificate
                    </div>
                    <ul class="section-list">
                      
                      <li data-lecture-id="21565364" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21565364" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21565364" data-ss-event-type="link" data-ss-lecture-id="21565364" data-ss-position="20" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21565364" id="sidebar_link_21565364">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Code"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Course Evaluation
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                      <li data-lecture-id="21651116" data-lecture-url="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21651116" class="section-item incomplete">
                        <a class="item" data-no-turbolink="true" data-ss-course-id="856463" data-ss-event-name="Lecture: Navigation Sidebar" data-ss-event-href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21651116" data-ss-event-type="link" data-ss-lecture-id="21651116" data-ss-position="20" data-ss-school-id="95353" data-ss-user-id="69497577" href="/courses/nutrition-integrative-medicine-for-diabetes-cognitive-decline-alzheimer-s-disease/lectures/21651116" id="sidebar_link_21651116">
                          <span class="status-container">
                            <span class="status-icon">
                              &nbsp;
                            </span>
                          </span>
                          <div class="title-container">
                            <span class="lecture-icon v-middle">
                                <svg width="24" height="24">
                                  <use xlink:href="#icon__Subject"></use>
                                </svg>
                            </span>
                            <span class="lecture-name">
                              Farewell
                              
                            </span>
                          </div>
                        </a>
                      </li>
                      
                    </ul>
                  </div>
                  
                </div>
              </div>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>