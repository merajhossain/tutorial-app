<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
        <!--begin::Menu Container-->
        <div id="kt_aside_menu" class="aside-menu" data-menu-vertical="1" data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <div class="course-sidebar">
                <!-- Image & Title -->
                <img class="course-image" src="https://process.fs.teachablecdn.com/ADNupMnWyR7kCWRvm76Laz/resize=width:705/https://www.filepicker.io/api/file/ZZB671PQW6hnOjwyRIsw">
                <h2>Nutrition &amp; Integrative Medicine for Diabetes, Cognitive Decline &amp; Alzheimer’s Disease</h2>
                <!-- Course Progress -->
                <div class="progress-full">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="course-listing-extra-info" aria-hidden="false">
                      <div>
                        <!-- Bundle Info (everyone) -->
                          <!-- Author Image and Name (everyone) -->
                          <img class="img-circle max-h-30px" src="http://127.0.0.1:8000/assets/images/m-mojibul-.jpg" alt="Dr. Mojibul Haque">
                          <span class="small course-author-name">
                            Dr. Mojibul Haque
                          </span>
                      </div>
                      <!-- Progress percentage (enrolled users) -->
                      <div>
                          <div class="" aria-hidden="false">
                              <div class="small course-progress">
                                <span class="percentage" id="percent-complete-856463" data-course-id="856463">23%</span>
                                <br>
                                COMPLETE
                              </div>
                          </div>
                            <!-- Price (unenrolled users) -->
                      </div>
                    </div>
                </div>
                <ul class="sidebar-nav">
                    <!-- Sidebar Nav -->
                
                  <li class="selected">
                    <a href="/course-curriculum" class="sidebar-nav-link">
                      <span class="lecture-sidebar-icon v-middle">
                        <i class="fas fa-list"></i>
                      </span>
                      Course Curriculum
                    </a>
                  </li>
                
                
                <li class="">
                  <a href="/course-instructor" class="sidebar-nav-link">
                    <span class="lecture-sidebar-icon v-middle">
                        <i class="fas fa-user"></i>
                    </span>
                    Your Instructor
                  </a>
                </li>
                
              
                </ul>
              </div>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>