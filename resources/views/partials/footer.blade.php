<footer class="bottom-menu bottom-menu-inverse">
    <div class="container">
      <div class="row">
        <div class="col-md-12 ">
            <div class="footer-wrapper w-100">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 footer-column text-center">
                  <a href="https://uih.education">
                    © The University of Integrated Health (UIH) 
                  </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 footer-column text-center">
                  <ul class="list-unstyled">
                    <li>
                      <a href="#">
                        Terms of Use
                      </a>
                    </li>
                    
                  </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 footer-column text-center">
                  
                    <ul class="list-unstyled">
                      
                      <li>
                        <a href="#">
                          Privacy Policy
                        </a>
                      </li>          
                      
                    </ul>
                  
                  
                  </div>
              </div>
        
              </div>
            </div>
        </div>
      </div>
    </footer>