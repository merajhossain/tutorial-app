
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
        @include('partials.headerScript')
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
		<!-- Start Content-->
        @yield('content')
        <!-- End Content-->
		@include('partials.footerScript')
	</body>
	<!--end::Body-->
</html>