<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.login');
});
Route::get('/tutorial-list', function () {
    return view('pages.tutorialListView');
});
Route::get('/course-curriculum', function () {
    return view('pages.tutorialCourseCurriculum');
});
Route::get('/full-width', function () {
    return view('pages.fullWidth');
});

Route::get('/course-lecture', function () {
    return view('pages.courseLecture');
});

Route::get('/course-instructor', function () {
    return view('pages.tutorialCourseInstructor');
});
